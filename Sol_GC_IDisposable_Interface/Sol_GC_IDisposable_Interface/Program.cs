﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_GC_IDisposable_Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            //AB _abobj = new AB();
            //_abobj.TestMethodA();
            //_abobj.TestMethodB();

            //_abobj.Dispose();

            
            //using block   //if exception occur in using block while using try-catch,then using block will call dispose method to release managed and unmanaged                                    resources

            using (AB abObj=new AB())
            {
                abObj.TestMethodA();
                abObj.TestMethodB();
            }
        }
    }

    public class A:IDisposable
    {
        private static A _aObj = null;

        private A()
        {
            //_aObj = new A();
            //Console.WriteLine("A-create constructor");
        }

        public void TestMethodA()
        {
            Console.WriteLine("Test method A");
        }

        public void Dispose()
        {
            _aObj = null;
            //GC.SuppressFinalize(this);
        }

        public static A CreateInstanceA()
        {
            return _aObj = new A();             
        }

        ~A()
        {
            Console.WriteLine("destroy A object");
        }
    }

    public class B: IDisposable
    {
        private static B _bObj = null;

        private B()
        {
            //_bObj = new B();
            //Console.WriteLine("B-create constructor");
        }

        public void TestMethodB()
        {
            Console.WriteLine("Test method B");
        }

        public void Dispose()
        {
            _bObj = null;
            GC.SuppressFinalize(this);
        }

        public static B CreateInstanceB()
        {
            return _bObj = new B();
        }

        ~B()
        {
            Console.WriteLine("destroy B object");
        }
    }

    public class AB:IDisposable
    {
        private A _aobj = null;
        private B _bobj = null;

        public AB()
        {
            _aobj = A.CreateInstanceA();
            _bobj = B.CreateInstanceB();

        }

        public void TestMethodA()
        {
            _aobj?.TestMethodA();           
            //Console.WriteLine("Test method B");
        }

        public void TestMethodB()
        {
            _bobj.TestMethodB();
            //Console.WriteLine("Test method B");
        }


        public void Dispose()
        {
            _aobj?.Dispose();
            _bobj?.Dispose();
            GC.SuppressFinalize(this);
        }

        ~AB()
        {
            Console.WriteLine("ab destroy object");
        }
    }
}
